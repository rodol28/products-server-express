const express = require('express');
const path = require('path');
const app = express();
const routes = require('./routes/index.js');
const bodyParser = require('body-parser');

//settings 
app.set('port', process.env.port || 3000);
app.set('views', path.join(__dirname, './views'));
app.set('view engine', 'ejs');

//middleware
app.use((req, res, next) => {
    console.log(`${req.url} - ${req.method}`);
    next();
});

app.use(bodyParser.json()); //permite entender la informacion que llega desde el navegador
app.use(bodyParser.urlencoded({extended: false})); //permite entender la informacion que llega desde un formulario html, (extended: false -> porque no se enviaran imagenes ni archivos complejos)


//routes
app.use(routes);


//static files
app.use(express.static(path.join(__dirname, './src/public')));

app.listen(app.get('port'), () => {
    console.log('server listening on port '+ app.get('port'));
});